# garden-demo

This repo is prepared to give a demo of using garden to manage development environments.

## Getting started

There are scripts included to install pre-requisite tooling and create a local dev cluster:

``` sh
$ ./scripts/prepare-requirements.sh
$ ./scripts/setup-local-cluster.sh
```
