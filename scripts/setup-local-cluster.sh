#!/usr/bin/env sh
set -e

echo "Create kind cluster.."
kind create cluster --config=scripts/kind-cluster-config.yaml

echo "Waiting 15s for cluster to spin up.."
sleep 15

echo "Deploying nginx ingress controller.."
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace \
  --set controller.service.type=NodePort --set controller.service.nodePorts.http=30080  --set controller.service.nodePorts.https=30443

echo "Wait for ingress controller to be avilable.."
kubectl wait --namespace ingress-nginx \
 --for=condition=ready pod \
 --selector=app.kubernetes.io/component=controller \
 --timeout=90s
