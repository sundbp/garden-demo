#!/usr/bin/env sh
set -e

echo "Installing kind.."
go install sigs.k8s.io/kind@v0.17.0
echo "kind installed."

echo "Installing helm.."
yay -S helm

echo "Installing garden.."
curl -sL https://get.garden.io/install.sh | bash
echo "garden installed."

echo "ensure your path is updated to contain $HOME/go/bin and $HOME/.garden/bin"
